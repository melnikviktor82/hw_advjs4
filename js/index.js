// Теоретичне питання

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// Asynchronous JavaScript And XML - це асинхронний JS, який ми використовуємо для роботи з сервером щоб
// надсилати запити на сервер і завантажувати нову інформацію. 

// Завдання

// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:

// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни

// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму,
// а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

const starWarsAPI = "https://ajax.test-danit.com/api/swapi/";

class Request {
  get(url) {
    return fetch(url).then((response) => response.json());
  }
}

class StarWarsUniverse {
  getFilmsInfo() {
    const request = new Request();
    return request.get(starWarsAPI + "films").then((jsonResponse) => {
      jsonResponse.sort((a, b) => a.episodeId - b.episodeId);

      const filmsList = document.createElement("ul");
      filmsList.classList.add("filmsList");

      const filmListItems = jsonResponse.map(
        ({ episodeId, name, openingCrawl, characters }) => {
          const listItem = document.createElement("li");

          listItem.innerHTML = `<p> <b>Episode ${episodeId} - ${name}:</b> <br><br> "${openingCrawl}" </p>`;

          this.getCharactersInfo(characters).then((charList) => {
            listItem.append(charList);
          });
          return listItem;
        }
      );
      filmsList.append(...filmListItems);
      return filmsList;
    });
  }

  getCharactersInfo(urls) {
    const promises = urls.map((url) => {
      const getCharacter = new Request();
      return getCharacter.get(url);
    });

    return Promise.all(promises)
      .then((jsonResponse) => jsonResponse.map((obj) => obj.name))
      .then((charArr) => {
        const charList = document.createElement("ul");
        charList.innerHTML = `Characters: <br><br>`.bold();

        const charListItems = charArr.map((name) => {
          const listItem = document.createElement("li");
          listItem.textContent = name;
          return listItem;
        });
        charList.append(...charListItems);

        return charList;
      });
  }
}

const btn = document.createElement("button");
btn.textContent = "Star Wars Films";
document.body.append(btn);

btn.addEventListener("click", function fn() {
  const starWarsFilms = new StarWarsUniverse();
  starWarsFilms.getFilmsInfo().then((data) => document.body.append(data));
  btn.removeEventListener("click", fn);
});
